package game;

import controller.PlatformController;
import model.Mario;
import utils.Constants;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    public Keyboard(){
    }

    @Override
    public void keyPressed(KeyEvent e) {

        Mario mario = PlatformController.marioController().mario();

        if (mario.alive()) {

            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (PlatformController.xPosition() == Constants.INITIAL_POS_PLATFORM) {
                    PlatformController.xPosition_$eq(0);
                    PlatformController.backgroundPosX_$eq(Constants.INITIAL_POS_BACKGROUND);
                }
                mario.moving_$eq(true);
                mario.toRight_$eq(true);
                PlatformController.movement_$eq(Constants.SX); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (PlatformController.xPosition() == Constants.FINAL_POS_PLATFORM + 1) {
                    PlatformController.xPosition_$eq(Constants.FINAL_POS_PLATFORM);
                    PlatformController.backgroundPosX_$eq(Constants.INITIAL_POS_BACKGROUND);
                }

                mario.moving_$eq(true);
                mario.toRight_$eq(false);
                PlatformController.movement_$eq(Constants.DX); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode()==KeyEvent.VK_SPACE) {
                PlatformController.marioController().mario().jumping_$eq(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        PlatformController.marioController().mario().moving_$eq(false);
        PlatformController.movement_$eq(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}

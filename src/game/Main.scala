package game

import java.awt.Toolkit

import controller.PlatformController
import utils.Constants
import view.Platform
import javax.swing._

object Main extends App {
    val window: JFrame = new JFrame(Constants.WINDOW_TITLE)
    window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    window.setSize(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT)
    window.setLocationRelativeTo(null)
    window.setResizable(true)
    window.setAlwaysOnTop(true)
    val scene: Platform = PlatformController.getPlatform
    window.setContentPane(scene)
    window.setVisible(true)
    val timer: Thread = new Thread(new Refresh)
    timer.start()

}
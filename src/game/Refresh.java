package game;

import controller.PlatformController;
import utils.Constants;

public class Refresh implements Runnable {



    public void run() {
        while (true) {
            PlatformController.getPlatform().repaint();
            try {
                Thread.sleep(Constants.PAUSE);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

} 

package utils;

import model.basics.Coordinates;

/**
 * Constants class
 * Created by sapi9 on 11/03/2017.
 */
public class Constants {
    public static final int FLAG_X_POS = 4650;
    public static final int CASTLE_X_POS = 4850;
    public static final int FLAG_Y_POS = 115;
    public static final int CASTLE_Y_POS = 145;

    public static final int PROXIMITY_MARGIN = 10;
    public static final int DELTA_HIT =5;

    //ENEMY
    public static final int ENEMY_PAUSE = 15;

    //MAIN
    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";

    //REFRESH
    public static final int PAUSE = 3;

    //BLOCK
    public static final int BLOCK_WIDTH = 30;
    public static final int BLOCK_HEIGHT = 30;
    public static final int BLOCKS_NUM = 12;
    public static final Coordinates[] BLOCKS_COORDINATES_1 = {

            new Coordinates(400, 180),
            new Coordinates(1200, 180),
            new Coordinates(1270, 170),
            new Coordinates(1340, 160),
            new Coordinates(2000, 180),
            new Coordinates(2600, 160),
            new Coordinates(2650, 180),
            new Coordinates(3500, 160),
            new Coordinates(3550, 140),
            new Coordinates(4000, 170),
            new Coordinates(4200, 200),
            new Coordinates(4300, 210),

    };
    public static final Coordinates[] BLOCKS_COORDINATES_2= {

            new Coordinates(400, 180),
            new Coordinates(1200, 180),
            new Coordinates(1540, 160),
            new Coordinates(2000, 180),
            new Coordinates(2600, 160),
            new Coordinates(2650, 180),
            new Coordinates(3000, 120),
            new Coordinates(3100, 160),
            new Coordinates(3350, 70),
            new Coordinates(4000, 70),
            new Coordinates(4200, 200),
            new Coordinates(4300, 210),

    };

    public static final Coordinates[] BLOCKS_COORDINATES_3= {

            new Coordinates(300, 180),
            new Coordinates(1200, 180),
            new Coordinates(1540, 160),
            new Coordinates(2000, 180),
            new Coordinates(2100, 160),
            new Coordinates(2250, 180),
            new Coordinates(2900, 120),
            new Coordinates(3000, 160),
            new Coordinates(1650, 90),
            new Coordinates(4000, 90),
            new Coordinates(4100, 230),
            new Coordinates(4300, 210),

    };

    //PIECE
    public static final int PIECE_WIDTH = 30;
    public static final int PIECE_HEIGHT = 30;
    public static final int PIECE_PAUSE = 10;
    public static final int FLIP_FREQUENCY = 100;
    public static final int PIECES_NUM = 10;
    public static final Coordinates[] PIECES_COORDINATES = {
            new Coordinates(402, 145),
            new Coordinates(1202, 140),
            new Coordinates(1272, 95),
            new Coordinates(1342, 40),
            new Coordinates(1650, 145),
            new Coordinates(2650, 145),
            new Coordinates(3000, 135),
            new Coordinates(3400, 125),
            new Coordinates(4200, 145),
            new Coordinates(4600, 40),

    };
    public static final Coordinates[] PIECES_COORDINATES_2 = {
            new Coordinates(402, 145),
            new Coordinates(1462, 10),
            new Coordinates(1172, 95),
            new Coordinates(1242, 95),
            new Coordinates(1650, 145),
            new Coordinates(2750, 145),
            new Coordinates(3100, 135),
            new Coordinates(3340, 125),
            new Coordinates(4100, 145),
            new Coordinates(4600, 40),

    };
    public static final Coordinates[] PIECES_COORDINATES_3 = {
            new Coordinates(402, 145),
            new Coordinates(1462, 110),
            new Coordinates(1172, 125),
            new Coordinates(1242, 235),
            new Coordinates(1650, 145),
            new Coordinates(2750, 145),
            new Coordinates(3100, 135),
            new Coordinates(3350, 125),
            new Coordinates(4100, 145),
            new Coordinates(4600, 40),

    };

    //TUNNEL
    public static final int TUNNEL_WIDTH = 43;
    public static final int TUNNEL_HEIGHT = 65;
    public static final int TUNNELS_NUM = 8;
    public static final Coordinates[] TUNNEL_COORDINATES = {
            new Coordinates(600, 230),
            new Coordinates(1000, 230),
            new Coordinates(1600, 230),
            new Coordinates(1900, 230),
            new Coordinates(2500, 230),
            new Coordinates(3000, 230),
            new Coordinates(3800, 230),
            new Coordinates(4500, 230),

    };
    public static final Coordinates[] TUNNEL_COORDINATES_2 = {
            new Coordinates(600, 230),
            new Coordinates(1300, 225),
            new Coordinates(1900, 220),
            new Coordinates(2300, 230),
            new Coordinates(3300, 240),
            new Coordinates(3700, 230),
            new Coordinates(4500, 230),
            new Coordinates(4600, 250),

    };

    public static final Coordinates[] TUNNEL_COORDINATES_3 = {
            new Coordinates(200, 230),
            new Coordinates(1100, 245),
            new Coordinates(1900, 230),
            new Coordinates(2300, 230),
            new Coordinates(3100, 250),
            new Coordinates(3700, 230),
            new Coordinates(4500, 230),
            new Coordinates(4600, 250),

    };

    //MARIO
    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int MARIO_WIDTH = 28;
    public static final int MARIO_HEIGHT = 50;
    public static final int JUMPING_LIMIT = 42;
    public static final int MARIO_X = 300;
    public static final int MARIO_Y = 245;
    public static final int MARIO_FREQUENCY = 25;


    //ENEMIES
    public static final int MUSH_WIDTH = 27;
    public static final int MUSH_HEIGHT = 30;
    public static final int TURTLE_WIDTH = 43;
    public static final int TURTLE_HEIGHT = 50;
    public static final int BIRD_WIDTH = 43;
    public static final int BIRD_HEIGHT = 43;
    public static final int INITIAL_POS_PLATFORM = -1;
    public static final int FINAL_POS_PLATFORM = 4600;
    public static final int INITIAL_POS_BACKGROUND = -50;
    public static final int SX =1;
    public static final int DX=-1;
    public static final int[] MUSH_X= {800};
    public static final int[] MUSH_Y= {263};
    public static final int[] TURTLE_X= {950, 1320, 2550};
    public static final int[] TURTLE_Y= {243, 243, 243};
    public static final int[] BIRD_X= {3600};
    public static final int[] BIRD_Y= {65};

    public static final int MUSHROOM_FREQUENCY = 45;
    public static final int TURTLE_FREQUENCY = 45;
    public static final int BIRD_FREQUENCY = 45;
    public static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    public static final int TURTLE_DEAD_OFFSET_Y = 30;
    public static final int BIRD_DEAD_OFFSET_Y = 30;


    public static final String SCORE ="Score:";
    public static final int SCORE_INCREMENT = 100;
    public static final int SCORE_FONT_SIZE = 16;
    public static final String SCORE_FONT = "Courier New";

    //LEVEL
    public static final int LEVEL_1 = 1;
    public static final int LEVEL_2 = 2;
    public static final int LEVEL_3 = 3;

    public static final String IMG_UNDERSCORE ="_";
    public static final int DELTA_END = 24;
    public static final int X_END =4600;
    public static final int X_CHANGE_BACKGROUND =-1600;
    public static final int X_CASTLE_BEGIN =10;
    public static final int X_START_BEGIN =220;
    public static final int X_BACKGROUND_BEGIN =-50;
    public static final int Y_CASTLE =95;
    public static final int Y_START =234;
}

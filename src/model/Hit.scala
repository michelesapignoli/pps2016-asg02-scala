package model

/**
  *
  */
object Hit{
  sealed trait Hit

  case object AHEAD extends Hit
  case object BACK extends Hit
  case object BELOW extends Hit
  case object ABOVE extends Hit
  case object NOPE extends Hit
}


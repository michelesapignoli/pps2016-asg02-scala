package model

import model.basics.BaseCharacter
import model.basics.Coordinates
import model.basics.Dimensions
import utils.Constants

class Mario(coordinates: Coordinates) extends BaseCharacter(coordinates,
          new Dimensions(Constants.MARIO_WIDTH, Constants.MARIO_HEIGHT)) {
  var jumping = false
  var jumpingExtent = 0

  def increaseJumping(): Unit = jumpingExtent += 1

}
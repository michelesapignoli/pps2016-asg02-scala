package model

import java.awt.Image

import model.basics.{AbstractEnemy, Coordinates}
import utils.{Constants, Res, Utils}

/**
  * Classes Mushroom and Turtle.
  */
class Mushroom(coordinates: Coordinates, frequency: Int, image: String, deadOffset: Int)
  extends AbstractEnemy(coordinates, Constants.MUSH_WIDTH, Constants.MUSH_HEIGHT, frequency, image, deadOffset) {
  def deadImage: Image = {
    Utils.getImage(if (this.toRight) Res.IMG_MUSHROOM_DEAD_DX
    else Res.IMG_MUSHROOM_DEAD_SX)
  }
}

class Turtle(coordinates: Coordinates, frequency: Int, image: String, deadOffset: Int)
  extends AbstractEnemy(coordinates, Constants.TURTLE_WIDTH, Constants.TURTLE_HEIGHT, frequency, image, deadOffset) {
  def deadImage: Image = {
    Utils.getImage(Res.IMG_TURTLE_DEAD)
  }
}

class Bird(coordinates: Coordinates, frequency: Int, image: String, deadOffset: Int)
  extends AbstractEnemy(coordinates, Constants.BIRD_WIDTH, Constants.BIRD_HEIGHT, frequency, image, deadOffset) {
  def deadImage: Image = {
    Utils.getImage(Res.IMG_BIRD_DEAD)
  }
}

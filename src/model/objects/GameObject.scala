package model.objects

import java.awt.Image
import controller.PlatformController
import model.basics.Coordinates
import model.basics.Sprite
import model.basics.Dimensions

abstract class GameObject(coordinates: Coordinates, dimensions: Dimensions) extends Sprite(coordinates, dimensions) {
  var imgObj: Image = _

  def imageOnMovement: Image


  def move() {
    if (PlatformController.xPosition >= 0) {
      this.coordinates.x = this.coordinates.x - PlatformController.movement
    }
  }
}
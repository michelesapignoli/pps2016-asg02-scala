package model.objects

import model.basics.Coordinates
import model.basics.Dimensions
import utils.Constants
import utils.Res
import utils.Utils
import java.awt.Image

class Coin(coordinates: Coordinates) extends GameObject(coordinates, new Dimensions(Constants.PIECE_WIDTH, Constants.PIECE_HEIGHT)) with Runnable {
  this.imgObj = Utils.getImage(Res.IMG_PIECE1)
  var counter: Int = 0

  def imageOnMovement: Image = {
    Utils.getImage(if ( {
      this.counter += 1; this.counter
    } % Constants.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  def run() {
    while (true) {
      {
        this.imageOnMovement
        try {
          Thread.sleep(Constants.PIECE_PAUSE)
        }
        catch {
          case e: InterruptedException => {
            System.out.println(e.getMessage)
          }
        }
      }
    }
  }
}
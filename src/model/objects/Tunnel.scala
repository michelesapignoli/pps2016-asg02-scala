package model.objects

import model.basics.Coordinates
import model.basics.Dimensions
import utils.Constants
import utils.Res
import utils.Utils
import java.awt._

class Tunnel(coordinates: Coordinates, level:Int) extends GameObject(coordinates, new Dimensions(Constants.TUNNEL_WIDTH, Constants.TUNNEL_HEIGHT)) {
  this.imgObj = Utils.getImage(Res.IMG_BASE + Res.IMG_TUNNEL + Constants.IMG_UNDERSCORE + level + Res.IMG_EXT)

  def imageOnMovement: Image = {
    null
  }
}
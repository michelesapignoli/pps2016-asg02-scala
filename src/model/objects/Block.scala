package model.objects

import model.basics.Coordinates
import model.basics.Dimensions
import utils.Constants
import utils.Res
import utils.Utils
import java.awt._

class Block(coordinates: Coordinates, level:Int) extends GameObject(coordinates, new Dimensions(Constants.BLOCK_WIDTH, Constants.BLOCK_HEIGHT)) {

  imgObj = Utils.getImage(Res.IMG_BASE + Res.IMG_BLOCK + Constants.IMG_UNDERSCORE + level + Res.IMG_EXT)

  def imageOnMovement: Image = {
    null
  }
}
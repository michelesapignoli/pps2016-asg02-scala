package model.basics


/**
  * BaseCharacter, extends Sprite
  */
class BaseCharacter(coordinates: Coordinates, dimensions: Dimensions) extends Sprite(coordinates, dimensions) {
  var moving = false
  var toRight = true
  var alive = true
  var counter = 0
  var offset: Int = 0

}
package model.basics

import controller.SpriteController
import utils.Constants
import java.awt._

/**
  * AbstractEnemy. Turtle and Mushroom extends from this.
  */
abstract class AbstractEnemy(coordinates: Coordinates, val width: Int, val height: Int, val frequency: Int, val image: String, val deadOffset: Int)
  extends BaseCharacter(coordinates, new Dimensions(width, height)) with Runnable {
  this.toRight = true
  this.moving = true
  this.offset = 1
  val movement: Thread = new Thread(this)
  movement.start()

  def run() {
    while (true) {

        if (this.alive) {
          SpriteController.move(this)
          try {
            Thread.sleep(Constants.ENEMY_PAUSE)
          }
          catch {
            case e: InterruptedException => {
              System.out.println(e.getMessage)
            }
          }
        }

    }
  }

  def deadImage: Image
}
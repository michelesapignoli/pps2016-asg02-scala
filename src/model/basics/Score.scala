package model.basics

import utils.Constants
import java.util.Observable

/**
  * Observable score. Part of Observer pattern used to refresh the score.
  */
class Score() extends Observable {
  private var score: Int = 0

  /**
    * Metod called whenever a coin is gained by Mario.
    *
    * @param score score
    */
  def changeData(score: Int) {
    this.score = score
    setChanged() // the two methods of Observable class
    notifyObservers(score)
  }

  def incrementScore: Int = {
    this.score += Constants.SCORE_INCREMENT
    this.score
  }
}
package controller

import game.Audio
import model.basics.{AbstractEnemy, BaseCharacter, Coordinates, Score}
import view.Platform
import model.objects.Block
import model.objects.Coin
import model.objects.GameObject
import model.objects.Tunnel
import utils.Constants
import utils.Res
import java.awt._

import model.Mario

import scala.collection.mutable.ListBuffer

/**
  * PlatformController: class that manages the logic of all of the elements of the platform
  */
object PlatformController {

  var marioController: MarioController = new MarioController
  var enemiesController: EnemiesController = new EnemiesController
  private var platform: Platform = _
  private var objects: ListBuffer[GameObject] = _
  private var coins: ListBuffer[Coin]=_
  private var characters: ListBuffer[BaseCharacter] = _
  var backgroundPosX: Int = Constants.X_BACKGROUND_BEGIN
  var movement: Int = 0
  var xPosition: Int = -1
  var floorOffsetY: Int = Constants.FLOOR_OFFSET_Y_INITIAL
  var heightLimit: Int = 0
  var endPosition:Int = 0
  var level:Int = Constants.LEVEL_1
  //Observable object
  private var score: Score = _

  private def loadPlatform() {
    this.platform = new Platform()
    val enemies = this.enemiesController.birds ++ this.enemiesController.turtles ++ this.enemiesController.mushrooms
    this.initialize(ListBuffer.tabulate(Constants.BLOCKS_NUM)(i => new Block(Constants.BLOCKS_COORDINATES_1(i),Constants.LEVEL_1)) ++
      ListBuffer.tabulate(Constants.TUNNELS_NUM)(i => new Tunnel(Constants.TUNNEL_COORDINATES(i), Constants.LEVEL_1)),
      ListBuffer.tabulate(Constants.PIECES_NUM)(i => new Coin(Constants.PIECES_COORDINATES(i))),
      enemies.asInstanceOf[ListBuffer[BaseCharacter]])
    this.score = new Score
    score.addObserver(this.platform)
  }

  private def initialize(objectList: ListBuffer[GameObject], coins: ListBuffer[Coin], characters: ListBuffer[BaseCharacter]): Unit ={
    //Tabulate method for adding view objects
    this.objects=new ListBuffer[GameObject]
    this.objects = objectList
    this.coins = coins
    this.characters=new ListBuffer[BaseCharacter]
    this.characters += marioController.mario
    this.characters = this.characters ++ characters

  }
  /**
    * Paint method, called by Swing inside Platform
    */
  def paint(g: Graphics, imgBackground: Image, castle: Image, start: Image, imgFlag: Image, imgCastle: Image) {
    this.handleContact()
    // Moving fixed model.objects
    this.updateWithWalking(g, imgBackground, castle, start, imgFlag, imgCastle)
    this.drawMario(g)
    this.drawEnemies(g)
  }

  private def handleContact() {
    var enemies = this.characters
    this.characters.foreach { char =>
      enemies = enemies.filter(c => c != char)

      //Contact with objects
      this.objects.foreach { obj =>
        char match {
          case char: AbstractEnemy =>
            this.enemiesController.checkContact(char, obj)
            enemies = enemies.filter(c => !c.isInstanceOf[Mario])
          case char: Mario => this.marioController.checkContact(char, obj)
        }
      }
      this.coins.foreach{coin=>
        if (this.marioController.contactCoin(coin)) {
          Audio.playSound(Res.AUDIO_MONEY)
          this.coins-= coin
          //Change data for Observer pattern
          this.score.changeData(this.score.incrementScore)
        }
      }

      //Contact with enemies
      enemies.foreach { enemy =>
        char match {
          case char: AbstractEnemy =>
            enemies = characters.filter(c => !c.isInstanceOf[Mario])
            this.enemiesController.checkContact(char, enemy)
          case char: Mario => this.marioController.checkContact(char, enemy)
        }
      }
    }
  }

  private def drawMario(g: Graphics) {
    if (this.marioController.mario.jumping) g.drawImage(this.marioController.doJump(),
      this.marioController.mario.coordinates.x, this.marioController.mario.coordinates.y, null)
    else g.drawImage(this.marioController.walk(this.marioController.mario, Res.IMGP_CHARACTER_MARIO,
      Constants.MARIO_FREQUENCY), this.marioController.mario.coordinates.x, this.marioController.mario.coordinates.y, null)
  }

  private def drawEnemies(g: Graphics) {
    val enemies = characters.filter(c => c.isInstanceOf[AbstractEnemy]).asInstanceOf[ListBuffer[AbstractEnemy]]
    enemies.foreach{enemy=>
      if (enemy.alive)
        g.drawImage(this.enemiesController.walk(enemy, enemy.image, enemy.frequency),
          enemy.coordinates.x, enemy.coordinates.y, null)
      else
        g.drawImage(enemy.deadImage, enemy.coordinates.x,
          enemy.coordinates.y + enemy.deadOffset, null)
    }
  }

  private def updateWithWalking(g: Graphics, imageBackground: Image, castle: Image, start: Image, imgFlag: Image, imgCastle: Image) {
    this.updateBackgroundOnMovement()
    g.drawImage(imageBackground, this.backgroundPosX, 0, null)
    g.drawImage(castle, Constants.X_CASTLE_BEGIN - this.xPosition, Constants.Y_CASTLE, null)
    g.drawImage(start, Constants.X_START_BEGIN - this.xPosition, Constants.Y_START, null)
    this.objects.foreach { obj =>
      g.drawImage(obj.imgObj, obj.coordinates.x, obj.coordinates.y, null)
      /*if (obj.isInstanceOf[Coin]) {
        g.drawImage(obj.imageOnMovement, obj.coordinates.x, obj.coordinates.y, null)
      }*/
      this.canMove(obj)
    }

    this.coins.foreach{ coin=>
      g.drawImage(coin.imageOnMovement, coin.coordinates.x, coin.coordinates.y, null)
      this.canMove(coin)
    }

    val enemies = characters.filter(c => c.isInstanceOf[AbstractEnemy])
    enemies.foreach { enemy =>
      SpriteController.move(enemy)
    }

    this.endPosition = Constants.CASTLE_X_POS - this.xPosition
    g.drawImage(imgFlag, Constants.FLAG_X_POS - this.xPosition, Constants.FLAG_Y_POS, null)
    g.drawImage(imgCastle, endPosition, Constants.CASTLE_Y_POS, null)

    if((this.marioController.mario.coordinates.x == this.endPosition + Constants.DELTA_END) && this.level < Constants.LEVEL_3){
      this.newLevel()
    }
  }

  private def newLevel(): Unit ={

    this.marioController.mario.coordinates.x = Constants.MARIO_X
    this.marioController.mario.coordinates.y = Constants.MARIO_Y
    var b_coordinates: Array[Coordinates] = new Array[Coordinates](Constants.BLOCKS_NUM)
    var t_coordinates:Array[Coordinates]= new Array[Coordinates](Constants.TUNNELS_NUM)
    var c_coordinates: Array[Coordinates]= new Array[Coordinates](Constants.PIECES_NUM)
    this.level = this.level + 1
    level match{
      case Constants.LEVEL_2 =>
        b_coordinates = Constants.BLOCKS_COORDINATES_2
        t_coordinates = Constants.TUNNEL_COORDINATES_2
        c_coordinates = Constants.PIECES_COORDINATES_2
      case Constants.LEVEL_3 =>
        b_coordinates = Constants.BLOCKS_COORDINATES_3
        t_coordinates = Constants.TUNNEL_COORDINATES_3
        c_coordinates = Constants.PIECES_COORDINATES_3
    }

    this.enemiesController.newEnemies()
    val enemies = this.enemiesController.birds ++ this.enemiesController.turtles ++ this.enemiesController.mushrooms

    this.initialize(ListBuffer.tabulate(Constants.BLOCKS_NUM)(i => new Block(b_coordinates(i), this.level)) ++
      ListBuffer.tabulate(Constants.TUNNELS_NUM)(i => new Tunnel(t_coordinates(i), this.level)),
      ListBuffer.tabulate(Constants.PIECES_NUM)(i => new Coin(c_coordinates(i))),
        enemies.asInstanceOf[ListBuffer[BaseCharacter]])

    this.platform.newLevelPlatform(this.level)
    this.xPosition=0
  }

  private def canMove(gameObject: GameObject): Unit ={
    if (this.xPosition >= 0 && this.xPosition <= Constants.X_END) {
      gameObject.move()
    }
  }

  private def updateBackgroundOnMovement() {
    if (this.xPosition >= 0 && this.xPosition <= Constants.X_END) {
      this.xPosition = this.xPosition + this.movement
      // Moving the screen to give the impression that Mario is walking
      this.backgroundPosX = this.backgroundPosX - this.movement
    }
    if (this.backgroundPosX == Constants.X_CHANGE_BACKGROUND) {
      this.backgroundPosX = 0
    }
    else if (this.backgroundPosX == 0) {
      //Moving left
      this.backgroundPosX = Constants.X_CHANGE_BACKGROUND
    }
  }

  /**
    * Gets the Platform if loaded, if not it loads it
    *
    * @return Platform
    */
  def getPlatform: Platform = {
    if (this.platform == null) {
      this.loadPlatform()
    }
    this.platform
  }

}
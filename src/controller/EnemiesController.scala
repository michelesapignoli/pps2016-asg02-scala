package controller

import model.{Bird, Hit, Mushroom, Turtle}
import model.basics.Sprite
import model.basics.BaseCharacter
import model.basics.Coordinates
import utils.{Constants, Res}

import scala.collection.mutable.ListBuffer

/**
  * EnemiesController.
  * It extends SpriteController, as it handles the characters of Mushroom and Turtle.
  */
class EnemiesController() extends SpriteController {
  var turtles: ListBuffer[Turtle] =
    ListBuffer.tabulate(Constants.TURTLE_X.length)(i => new Turtle(new Coordinates(Constants.TURTLE_X(i), Constants.TURTLE_Y(i)), Constants.TURTLE_FREQUENCY, Res.IMGP_CHARACTER_TURTLE, Constants.TURTLE_DEAD_OFFSET_Y))
  var mushrooms: ListBuffer[Mushroom] =
    ListBuffer.tabulate(Constants.MUSH_X.length)(i => new Mushroom(new Coordinates(Constants.MUSH_X(i), Constants.MUSH_Y(i)),Constants.MUSHROOM_FREQUENCY, Res.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_DEAD_OFFSET_Y))
  var birds: ListBuffer[Bird] =
    ListBuffer.tabulate(Constants.BIRD_X.length)(i => new Bird(new Coordinates(Constants.BIRD_X(i), Constants.BIRD_Y(i)),Constants.BIRD_FREQUENCY, Res.IMGP_CHARACTER_BIRD, Constants.BIRD_DEAD_OFFSET_Y))

  /**
    * Method inherited by the abstract SpriteController,
    * as part of the template method of checkContact.
    * It handles the contact of the enemy with an obstacle.
    *
    * @param character enemy
    * @param obstacle  block
    */
  def contact(character: BaseCharacter, obstacle: Sprite) {
    this.hit(character, obstacle) match {
      case Hit.AHEAD =>
        if (character.toRight) {
          character.toRight_$eq(false)
          character.offset_$eq(-1)
        }

      case Hit.BACK =>
        if (!character.toRight) {
          character.toRight_$eq(true)
          character.offset_$eq(1)
        }

      case _=>
    }
  }

  def newEnemies(): Unit ={
    this.mushrooms.clear()
    this.birds.clear()
    this.turtles.clear()

    this.turtles =  ListBuffer.tabulate(Constants.TURTLE_X.length)(i => new Turtle(new Coordinates(Constants.TURTLE_X(i), Constants.TURTLE_Y(i)), Constants.TURTLE_FREQUENCY, Res.IMGP_CHARACTER_TURTLE, Constants.TURTLE_DEAD_OFFSET_Y))
    this.birds = ListBuffer.tabulate(Constants.BIRD_X.length)(i => new Bird(new Coordinates(Constants.BIRD_X(i), Constants.BIRD_Y(i)),Constants.BIRD_FREQUENCY, Res.IMGP_CHARACTER_BIRD, Constants.BIRD_DEAD_OFFSET_Y))
    this.birds = ListBuffer.tabulate(Constants.BIRD_X.length)(i => new Bird(new Coordinates(Constants.BIRD_X(i), Constants.BIRD_Y(i)),Constants.BIRD_FREQUENCY, Res.IMGP_CHARACTER_BIRD, Constants.BIRD_DEAD_OFFSET_Y))
  }
}
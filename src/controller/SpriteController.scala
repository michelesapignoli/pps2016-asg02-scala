package controller

import java.awt._

import model.Hit
import model.basics.{BaseCharacter, Sprite}
import utils.{Constants, Res, Utils}
import model.Hit._

/**
  * SpriteController: it handles the movements.
  */
object SpriteController {
  def move(character: BaseCharacter) {
    if (character.toRight)
      character.offset=Constants.SX
    else
      character.offset=Constants.DX

    character.coordinates.x= character.coordinates.x + character.offset
  }

}

abstract class SpriteController() {
  /**
    * Little template method with the subclasses
    *
    * @param character principal character
    * @param obstacle  obstacle
    */
  def checkContact(character: BaseCharacter, obstacle: Sprite) {
    if (this.isNearby(character, obstacle)) this.contact(character, obstacle)
  }

  def contact(character: BaseCharacter, obstacle: Sprite)

  def walk(character: BaseCharacter, name: String, frequency: Int): Image = {
    character.counter_$eq(character.counter + 1)
    val str: String = Res.IMG_BASE + name + (if (!character.moving || character.counter % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (character.toRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
    Utils.getImage(str)
  }


  def hit(mainCharacter: Sprite, obstacle: Sprite):Hit ={
    if(this.hitAhead(mainCharacter, obstacle))
      Hit.AHEAD
    else if(this.hitBelow(mainCharacter, obstacle))
      Hit.BELOW
    else if(this.hitBack(mainCharacter, obstacle))
      Hit.BACK
    else if(this.hitAbove(mainCharacter, obstacle))
      Hit.ABOVE
    else
      Hit.NOPE

  }

  def hitAhead(character: Sprite, obstacle: Sprite): Boolean = {
    !(sumX(character) < obstacle.coordinates.x ||
      sumX(character) > obstacle.coordinates.x + Constants.DELTA_HIT ||
      sumY(character) <= obstacle.coordinates.y ||
      character.coordinates.y >= sumY(obstacle))
  }

  def hitBack(character: Sprite, obstacle: Sprite): Boolean = {
    !(character.coordinates.x > sumX(obstacle) ||
      sumX(character) < obstacle.coordinates.x + obstacle.dimensions.width - Constants.DELTA_HIT ||
      sumY(character) <= obstacle.coordinates.y ||
      character.coordinates.y >= sumY(obstacle))
  }

  def hitBelow(character: Sprite, obstacle: Sprite): Boolean = {
    !(sumX(character) < obstacle.coordinates.x + Constants.DELTA_HIT ||
      character.coordinates.x > sumX(obstacle) - Constants.DELTA_HIT ||
      sumY(character) < obstacle.coordinates.y ||
      sumY(character) > obstacle.coordinates.y + Constants.DELTA_HIT)
  }

  def hitAbove(character: Sprite, obstacle: Sprite): Boolean = {
    !(character.coordinates.x + obstacle.dimensions.width < obstacle.coordinates.x + Constants.DELTA_HIT ||
      character.coordinates.x > sumX(obstacle) - Constants.DELTA_HIT ||
      character.coordinates.y < sumY(obstacle) ||
      character.coordinates.y > sumY(obstacle) + Constants.DELTA_HIT)
  }

  def isNearby(character: Sprite, obstacle: Sprite): Boolean = {
    if ((character.coordinates.x > obstacle.coordinates.x - Constants.PROXIMITY_MARGIN &&
      character.coordinates.x < sumX(obstacle) + Constants.PROXIMITY_MARGIN) ||
      (sumX(character) > obstacle.coordinates.x - Constants.PROXIMITY_MARGIN &&
        sumX(character) < sumX(obstacle) + Constants.PROXIMITY_MARGIN))
      return true
    false
  }

  private def sumX(sprite:Sprite): Int ={
    sprite.coordinates.x + sprite.dimensions.width
  }
  private def sumY(sprite:Sprite): Int ={
    sprite.coordinates.y + sprite.dimensions.height
  }
}
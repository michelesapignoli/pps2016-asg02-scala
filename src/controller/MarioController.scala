package controller

import model.{Hit, Mario}
import model.basics.Sprite
import model.basics.BaseCharacter
import model.basics.Coordinates
import utils.Constants
import utils.Res
import utils.Utils
import java.awt._

/**
  * MarioController
  * It extends SpriteController, as it handles the character of Mario.
  */
class MarioController() extends SpriteController {

  val mario: Mario = new Mario(new Coordinates(Constants.MARIO_X, Constants.MARIO_Y))

  /**
    * Method that allows Mario jumping
    *
    * @return image
    */
  def doJump(): Image = {
    var str: String = null
    this.mario.increaseJumping()
    if (this.mario.jumpingExtent < Constants.JUMPING_LIMIT) {
      if (this.mario.coordinates.y > PlatformController.heightLimit)
        this.mario.coordinates.y_$eq(this.mario.coordinates.y - 4)
      else this.mario.jumpingExtent_$eq(Constants.JUMPING_LIMIT)
      str = if (this.mario.toRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else if (this.mario.coordinates.y + this.mario.dimensions.height < PlatformController.floorOffsetY) {
      this.mario.coordinates.y_$eq(this.mario.coordinates.y + 1)
      str = if (this.mario.toRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (this.mario.toRight) Res.IMG_MARIO_ACTIVE_DX
      else Res.IMG_MARIO_ACTIVE_SX
      this.mario.jumping_$eq(false)
      this.mario.jumpingExtent_$eq(0)
    }
    Utils.getImage(str)
  }

  /**
    * Method inherited by the abstract SpriteController,
    * as part of the template method of checkContact.
    * It handles the contact of Mario with obstacles.
    *
    * @param character enemy
    * @param obstacle  obstacle
    */
  def contact(character: BaseCharacter, obstacle: Sprite) {
    this.hit(character, obstacle) match {

      case Hit.ABOVE =>
        ground()
        PlatformController.heightLimit = obstacle.coordinates.y + obstacle.dimensions.height // the new sky goes below the object

      case Hit.BELOW =>
        obstacle match {
          case enemy: BaseCharacter =>
            enemy.moving_$eq(false)
            enemy.alive_$eq(false)
            ground()
          case _ =>
            if (this.mario.jumping) PlatformController.floorOffsetY = obstacle.coordinates.y
        }
      case Hit.AHEAD =>
        obstacle match {
          case enemy: BaseCharacter =>
            canKillMario(enemy)
          case _ =>
            if (character.toRight) movPlatform()
            else {
              ground()
              resetSkyLimit()
            }
        }
      case Hit.BACK =>
        obstacle match {
          case enemy: BaseCharacter =>
            canKillMario(enemy)
          case _ =>
            if (!character.toRight) movPlatform()
            else {
              ground()
              resetSkyLimit()
            }
        }

      case Hit.NOPE =>
        if (!obstacle.isInstanceOf[BaseCharacter]) {
          ground()
        }
        resetSkyLimit()
    }

  }

  private def canKillMario(enemy: BaseCharacter): Unit = {
    if (enemy.alive) {
      this.mario.moving_$eq(false)
      this.mario.alive_$eq(false)
    }
  }

  private def ground(): Unit = {
    PlatformController.floorOffsetY = Constants.FLOOR_OFFSET_Y_INITIAL
    if (!this.mario.jumping) {
      this.mario.coordinates.y_$eq(Constants.MARIO_OFFSET_Y_INITIAL)
    }
  }

  private def movPlatform(): Unit = {
    PlatformController.movement = 0
    this.mario.moving_$eq(false)
  }


  private def resetSkyLimit(): Unit = {
    if (!this.mario.jumping) PlatformController.heightLimit = 0
  }

  def contactCoin(piece: Sprite): Boolean = {
    this.hit(this.mario, piece) match {
      case Hit.AHEAD | Hit.BELOW | Hit.ABOVE | Hit.BACK =>
        true
      case _ =>
        false
    }
  }
}